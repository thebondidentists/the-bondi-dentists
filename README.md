You will experience a comfortable and fresh atmosphere dedicated to your dental health needs from our friendly, professional, and dedicated team who will see to it that your appointment is as relaxing and as enjoyable as possible.

Address: Unit 605, Level 6, 35 Spring Street, Bondi Junction, Sydney, NSW 2022, Australia

Phone: +61 2 9389 0700